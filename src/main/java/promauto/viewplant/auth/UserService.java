package promauto.viewplant.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import promauto.viewplant.entity.User;
import promauto.viewplant.entity.Userrole;
import promauto.viewplant.repository.UserRepository;
import promauto.viewplant.repository.UserroleRepository;

import java.util.List;

@Service
public class UserService {
    UserRepository userRepository;
    UserroleRepository userroleRepository;

    @Autowired
    public UserService(UserRepository userRepository, UserroleRepository userroleRepository) {
        this.userRepository = userRepository;
        this.userroleRepository = userroleRepository;
    }


    public User getUser(String username) {
        return userRepository.findByUsername(username);
    }


    public List<Userrole> getUserroles(User user) {
        return userroleRepository.findAllByUserId(user.getId());
    }



    /*
    Map<String, User> users = new HashMap<>();

    public UserService() {
        User user;

        user = new User(1, "d", "Денис", "$2a$10$OygPm3hxlQIrQJ7a/vlmB.DZ3zwFWfAAedKIDjLhLbvshS4vy1bgC");
        user.getRolenames().add("admin");
        user.getRolenames().add("operator.shad.elv");
        users.put(user.getUsername(), user);

        user = new User(2, "a", "Иванов Иван Иваныч", "$2a$10$QB71nJsqHtFTbYyqLPfJZePmaAN03/SL1AojMoFOsLS3FksmATyDm");
        user.getRolenames().add("operator.shad.mill");

        users.put(user.getUsername(), user);
    }

    public User getUser(String username) {
        return users.get(username);
    }

    public void addUser(User user) {
        users.put(user.getUsername(), user);
    }

    public User removeUser(String username) {
        return users.remove(username);
    }
*/
}
