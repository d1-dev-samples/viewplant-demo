package promauto.viewplant.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import promauto.viewplant.config.ViewplantLoader;
import promauto.viewplant.entity.User;
import promauto.viewplant.entity.Userrole;
import promauto.viewplant.util.MapObjectFacade;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class AuthService implements UserDetailsService {
    public static final String ROLE_PREFIX = "ROLE_";

    private UserService userService;

    private Map<String, Role> roles = new HashMap<>();

    @Autowired
    public AuthService(ViewplantLoader viewplantLoader, UserService userService) {
        this.userService = userService;
        load( viewplantLoader.getConf() );
    }

    public Collection<Role> getRoles() {
        return roles.values();
    }

    public void load(Map<String, Object> conf) {
        MapObjectFacade m = new MapObjectFacade(conf.get("auth"));
        m = new MapObjectFacade(m.get("roles"));

        for (String name : m.keySet()) {
            Role role = new Role(name);
            roles.put(ROLE_PREFIX + role.getName(), role);

            MapObjectFacade mm = new MapObjectFacade(m.get(name));

            role.setDescr(mm.get("descr", ""));

            for (Object obj : mm.getList("filter"))
                role.addWildcardPattern(obj.toString());
        }
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userService.getUser(username);
        if( user == null )
            throw new UsernameNotFoundException("cannot find username: " + username);

        List<Userrole> userroles = userService.getUserroles(user);

        List<Role> roles1 = userroles.stream()
                .map(rolename -> roles.get(ROLE_PREFIX + rolename.getRolename().toUpperCase()))
                .filter(role -> role != null)
                .collect(Collectors.toList());

        return new AuthUserDetails(user, roles1);
    }


    public boolean isPermitted(String target) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if( authentication == null )
            return false;

        for(GrantedAuthority authority: authentication.getAuthorities()) {
            Role role = roles.get(authority.getAuthority());
            if( role == null)
                continue;

            if( role.isPermitted(target) )
                return true;
        }

        return false;
    }


    public User getCurrentUser() {
        User user = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if( authentication != null  &&  authentication.getPrincipal() instanceof AuthUserDetails) {
            AuthUserDetails ud = (AuthUserDetails) authentication.getPrincipal();
            user = ud.getUser();
        }
        return user;
    }
}
