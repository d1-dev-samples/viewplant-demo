package promauto.viewplant.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import promauto.viewplant.sidenav.SidenavItem;
import promauto.viewplant.sidenav.SidenavItemType;
import promauto.viewplant.sidenav.SidenavService;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static promauto.viewplant.sidenav.SidenavItemType.FOLDER;

@Component
public class AuthMatchers {
    private SidenavService sidenavService;
    private AuthService authService;


    public static class Item {
        String name;
        HttpMethod httpMethod;
        String[] regexs;
        String[] roles;

        public Item(String name, HttpMethod httpMethod, String[] regexs, String[] roles) {
            this.name = name;
            this.httpMethod = httpMethod;
            this.regexs = regexs;
            this.roles = roles;
        }

        public String getName() {
            return name;
        }

        public HttpMethod getHttpMethod() {
            return httpMethod;
        }

        public String[] getRegexs() {
            return regexs;
        }

        public String[] getRoles() {
            return roles;
        }
    }


    @Autowired
    public AuthMatchers(SidenavService sidenavService, AuthService authService) {
        this.sidenavService = sidenavService;
        this.authService = authService;
    }


    private void addSidenavMatchers(List<Item> items) {
        for(SidenavItem si: sidenavService.getItems()) {
            if( si.getSidenavItemType() == FOLDER ) continue;

            String name = si.getFullName();

            String[] regexs = new String[]{String.format("/%s.*=%s(&.*|$)",
                    si.getSidenavItemType().name().toLowerCase(),
                    si.getName().replace(".", "\\."))};

            List<String> rolelist = authService.getRoles().stream()
                    .filter(role -> role.isPermitted(name))
                    .map(role -> role.getName())
                    .collect(Collectors.toList());

            String[] roles = new String[ rolelist.size() ];
            rolelist.toArray(roles);

            items.add( new Item(name, HttpMethod.GET, regexs, roles) );
        }
    }


    public List<Item> getItems() {
        List<Item> items = new LinkedList<>();

        addSidenavMatchers(items);

        return items;
    }
}
