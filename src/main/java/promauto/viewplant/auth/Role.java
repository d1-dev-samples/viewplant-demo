package promauto.viewplant.auth;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class Role {
    private String name;
    private String descr;
    private List<Pattern> patterns = new ArrayList<>();


    public Role(String name) {
        this.name = name.toUpperCase();
    }

    public String getName() {
        return name;
    }


    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }


    public void addWildcardPattern(String filter) {
        try {
            String regex = filter
                    .replace(".", "\\.")
                    .replace("*", ".*")
                    .replace('?', '.')
                    .toUpperCase();

            patterns.add( Pattern.compile(regex) );
        }catch (PatternSyntaxException e) {
            //todo: log error
        }
    }


    public boolean isPermitted(String target) {
        return patterns.stream().anyMatch(p -> p.matcher(target.toUpperCase()).matches() );
    }


}
