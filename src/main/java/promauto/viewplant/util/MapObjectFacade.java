package promauto.viewplant.util;

import java.util.*;

public class MapObjectFacade {

    private final Map<String,Object> map;


    private void throwCastException(String expectedType, Object obj) {
        throw new ClassCastException("Object is not a " + expectedType + ": " + obj.getClass().getName() + ", " + obj.toString());
    }


    public MapObjectFacade(Object obj) {
        if( obj == null )
            this.map = new HashMap<>();
        else
        if( obj instanceof Map )
            this.map = (Map<String, Object>)obj;
        else {
            this.map = null;
            throwCastException("Map", obj);
        }
    }


    public Object get(String key) {
        return map.get(key);
    }

    public int get(String key, int value) {
        Object obj = map.get(key);
        if( obj == null )
            return value;

        if( !(obj instanceof Integer))
            throwCastException("Integer", obj);

        return (Integer)obj;
    }

    public double get(String key, double value) {
        Object obj = map.get(key);
        if( obj == null )
            return value;

        if( obj instanceof Double )
            return (Double)obj;

        if( obj instanceof Integer )
            return (Integer)obj;

        throwCastException("Double", obj);
        return 0;
    }

    public String get(String key, String value) {
        Object obj = map.get(key);
        if( obj == null )
            return value;

        return obj.toString();
    }


    public List<Object> getList(String key) {
        Object obj = map.get(key);
        if( obj == null )
            return new ArrayList<>();

        if( !(obj instanceof List))
            throwCastException("List", obj);

        return (List<Object>)obj;
    }

    public <K,V> Map<K,V> getMap(String key) {
        Object obj = map.get(key);
        if( obj == null )
            return new HashMap<>();

        if( !(obj instanceof Map))
            throwCastException("List", obj);

        return (Map<K,V>)obj;
    }


    public Set<String> keySet() {
        return map.keySet();
    }
}
