package promauto.viewplant.report;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.HtmlExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.type.WhenNoDataTypeEnum;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleHtmlExporterOutput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.j2ee.servlets.ImageServlet;
import net.sf.jasperreports.web.util.WebHtmlResourceHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import promauto.viewplant.config.ViewplantLoader;
import promauto.viewplant.databases.DatabaseManager;
import promauto.viewplant.databases.DatasourceNotFoundException;
import promauto.viewplant.util.MapObjectFacade;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static net.sf.jasperreports.engine.JRParameter.IS_IGNORE_PAGINATION;

@Service
public class ReportService {

    Logger logger = LoggerFactory.getLogger(ReportService.class);

    private DatabaseManager databaseManager;

    private Map<String, Report> reports = new HashMap<>();
    private String reportDir = "reports/";


    public ReportService() {
    }


    @Autowired
    public ReportService(ViewplantLoader viewplantLoader, DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
        load( viewplantLoader.getConf() );
    }


    public void load(Map<String, Object> conf) {
        MapObjectFacade m = new MapObjectFacade(conf.get("reports"));

        reportDir = m.get("report.dir", reportDir);
        m = new MapObjectFacade( m.get("report.items") );

        for (String name : m.keySet()) {
            Report report = new Report();
            reports.put(name, report);

            MapObjectFacade mm = new MapObjectFacade(m.get(name));

            report.setName(name);
            report.setCaption(          mm.get("caption", name));
            report.setDatabaseAlias(    mm.get("database", "primary"));
            report.setReportFileName(   mm.get("file", ""));
            String paramFilename = mm.get("param", "");
            report.loadParameters(reportDir, paramFilename );

            Map<String, String> vars = mm.getMap("vars");
            for (String var : vars.keySet())
                report.addVar(var, vars.get(var));
        }
    }


    public boolean show(
            Map<String,String> requestparams,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception
    {
        long timestart = System.currentTimeMillis();


        // process params
        String reportName = requestparams.getOrDefault("report", "");
        Report report = reports.get(reportName);
        if( report == null ) {
            logger.error("Report not found: " + reportName);
            return false;
        }

        String database = report.getDatabaseAlias();
        String export = requestparams.getOrDefault("export", "html").toLowerCase().trim();

        Map<String,Object> params = new HashMap<>(requestparams);
        params.putAll(report.getVars());

        if( export.equals("html"))
            params.put(IS_IGNORE_PAGINATION, true);

        // load and compile report
        JasperReport jasperReport = null;
        try {
            String filename = reportDir + report.getReportFileName();
            File f = new File(filename + ".jasper");
            if (f.exists()) {
                jasperReport = (JasperReport) JRLoader.loadObject(f);
            } else {
                f = new File(filename + ".jrxml");
                if (f.exists()) {
                    jasperReport = JasperCompileManager.compileReport( f.toString() );
                } else {
                    logger.error("Report file not found: " + filename);
                    return false;
                }
            }

            jasperReport.setWhenNoDataType(WhenNoDataTypeEnum.ALL_SECTIONS_NO_DETAIL);
        } catch (Exception e) {
            logger.error("Report loading error", e);
            return false;
        }


        // obtain database connection and fill report
        JasperPrint jasperPrint = null;
        try(Connection conn = databaseManager.get(database).getConnection()) {
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
            request.getSession().setAttribute(ImageServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE, jasperPrint);
        } catch (DatasourceNotFoundException | SQLException e) {
            logger.error("Report filling error: " + database, e);
            return false;
        }


        // export html
        if( export.equals("html")) {
            response.setContentType("text/html;charset=utf-8;");
            HtmlExporter exporter = new HtmlExporter(DefaultJasperReportsContext.getInstance());
            exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            SimpleHtmlExporterOutput output = new SimpleHtmlExporterOutput(response.getWriter());
            output.setImageHandler(new WebHtmlResourceHandler("/report/images?image={0}"));
            exporter.setExporterOutput(output);
            exporter.exportReport();
        }

        // export pdf
        if( export.equals("pdf")) {
            JRPdfExporter pdfExporter = new JRPdfExporter();
            pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream();
            pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
            pdfExporter.exportReport();

            response.setContentType("application/pdf");
            response.setHeader("Content-Length", String.valueOf(pdfReportStream.size()));
            response.addHeader("Content-Disposition", "inline; filename=" + report.getReportFileName() + ".pdf;");

            OutputStream responseOutputStream = response.getOutputStream();
            responseOutputStream.write(pdfReportStream.toByteArray());
            responseOutputStream.close();
            pdfReportStream.close();
        }



        long elapsedTimeMillis = System.currentTimeMillis() - timestart;
        System.out.println("elapsedTime: " + elapsedTimeMillis);
        return true;
    }


    public Report getReport(String report) {
        return reports.get(report);
    }


}
