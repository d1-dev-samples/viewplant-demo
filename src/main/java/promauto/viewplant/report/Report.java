package promauto.viewplant.report;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.viewplant.sidenav.SidenavItem;
import promauto.viewplant.sidenav.SidenavItemType;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Report implements SidenavItem {
    Logger logger = LoggerFactory.getLogger(Report.class);

    private int sidenavId;
    private SidenavItem sidenavParent = null;

    private String name;
    private String caption;
    private String reportFileName;
    private String databaseAlias;
    private String paramText; // TODO change to paramHtml
    private Map<String, String> vars = new HashMap<>();



    public Report() {
    }


    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }



    public String getReportFileName() {
        return reportFileName;
    }

    public void setReportFileName(String reportFileName) {
        this.reportFileName = reportFileName;
    }



    public String getDatabaseAlias() {
        return databaseAlias;
    }

    public void setDatabaseAlias(String databaseAlias) {
        this.databaseAlias = databaseAlias;
    }



    public void addVar(String key, String value) {
        vars.put(key, value);
    }

    public Map<String, String> getVars() {
        return vars;
    }



    public String getParamText() {
        return paramText;
    }



    public void loadParameters(String reportDir, String paramFilename) {
        paramFilename = paramFilename.isEmpty()? reportFileName: paramFilename;

        Path pathHtml = Paths.get(reportDir).resolve(paramFilename + ".html");
        if( Files.exists(pathHtml) ) {
            try {
                paramText = new String(Files.readAllBytes(pathHtml), StandardCharsets.UTF_8);
            } catch (IOException e) {
                logger.error("Report parameters file load error", e);
            }
        }
    }


    // sidenav stuff
    @Override
    public int getSidenavId() {
        return sidenavId;
    }


    @Override
    public void setSavenavId(int id) {
        this.sidenavId = id;
    }


    @Override
    public SidenavItem getSidenavParent() {
        return sidenavParent;
    }


    @Override
    public void setSidenavParent(SidenavItem parent) {
        this.sidenavParent = parent;
    }


    @Override
    public SidenavItemType getSidenavItemType() {
        return SidenavItemType.REPORT;
    }



}
