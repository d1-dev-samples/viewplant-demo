package promauto.viewplant.report.scriptlet;

import net.sf.jasperreports.engine.JRDefaultScriptlet;
import net.sf.jasperreports.engine.JRScriptletException;

public class HelloScriptlet extends JRDefaultScriptlet {
	
	public String hello(int i) throws JRScriptletException {
		return "Hello World! " + i/10;
	}

	public Integer period(int period) throws JRScriptletException {
		return period * 100 + 8;
	}

}
