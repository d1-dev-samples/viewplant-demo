package promauto.viewplant.sidenav;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import promauto.viewplant.chart.ChartService;
import promauto.viewplant.config.ViewplantLoader;
import promauto.viewplant.report.ReportService;

import java.util.*;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static promauto.viewplant.sidenav.SidenavItemType.*;

@Service
public class SidenavService {

    private List<SidenavItem> items = new ArrayList<>();
    private ReportService reportService;
    private ChartService chartService;

    @Autowired
    public SidenavService(
            ViewplantLoader viewplantLoader,
            ReportService reportService,
            ChartService chartService
    ) {
        this.reportService = reportService;
        this.chartService = chartService;
        load( viewplantLoader.getConf() );
    }

    public void load(Map<String, Object> conf) {

        List<Object> confSidenav = (List<Object>)conf.get("sidenav");

        if( confSidenav != null) {
            loadItems(null, confSidenav);
        }

        // setting up ids
        int i = 1;
        for (SidenavItem item: items)
            item.setSavenavId(i++);
    }



    private void loadItems(SidenavItem parent, Object conf) {
        if( !(conf instanceof List<?>) )
            return;

        List<Object> list = (List<Object>)conf;

        for(Object obj: list) {
            if( obj instanceof Map<?,?> ) {
                Map<String,Object> map = (Map<String,Object>)obj;
                String name = map.keySet().iterator().next();
                Object confChildren = map.get(name);

                SidenavFolder item = new SidenavFolder(name, parent);
                items.add(item);
                loadItems(item, confChildren);
            }

            if( obj instanceof String ) {
                SidenavItem item = getItemByName(obj.toString());
                if( item != null ) {
                    item.setSidenavParent(parent);
                    items.add(item);
                }
            }
        }
    }


    public List<SidenavItem> getItems() {
        return items;
    }


    private SidenavItem getItemByName(String itemName) {
        SidenavItem item = null;
        Matcher m = Pattern.compile("(\\w+):(\\S.*\\S)").matcher(itemName);
        if( m.find() ) {
            String type = m.group(1).toUpperCase();
            String name = m.group(2);

            if( type.equals(REPORT.name()) )
                item = reportService.getReport(name);

            if( type.equals(CHART.name()) )
                item = chartService.getChart(name);

        }
        return item;
    }


    public List<SidenavItem> removeEmptyFolders(List<SidenavItem> items) {

        Set<Integer> goodItems = new HashSet<>();

        for(SidenavItem si: items) {
            if( si.getSidenavItemType() == FOLDER ) continue;

            do {
                goodItems.add( si.getSidenavId() );
                si = si.getSidenavParent();
            } while (si != null );
        }

        return items.stream()
                .filter(si -> goodItems.contains(si.getSidenavId()))
                .collect(Collectors.toList());
    }


}