package promauto.viewplant.sidenav;

public enum SidenavItemType {
    FOLDER,
    REPORT,
    CHART
}
