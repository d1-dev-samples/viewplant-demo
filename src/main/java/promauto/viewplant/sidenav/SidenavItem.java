package promauto.viewplant.sidenav;


public interface SidenavItem {

    int getSidenavId();

    void setSavenavId(int id);

    SidenavItem getSidenavParent();

    void setSidenavParent(SidenavItem parent);

    SidenavItemType getSidenavItemType();

    String getName();


    default  int getSidenavParentId() {
        return getSidenavParent() == null? 0: getSidenavParent().getSidenavId();
    }

    default  String getFullName() {
        return getSidenavItemType().name().toLowerCase() + ':' + getName().toLowerCase();
    }

}
