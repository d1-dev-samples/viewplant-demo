package promauto.viewplant.sidenav;


public class SidenavFolder implements SidenavItem {
    private int id;
    private String name;
    private SidenavItem parent = null;

    public SidenavFolder() {
    }

    public SidenavFolder(String name, SidenavItem parent) {
        this.name = name;
        this.parent = parent;
    }

    @Override
    public int getSidenavId() {
        return id;
    }

    @Override
    public void setSavenavId(int id) {
        this.id = id;
    }

    @Override
    public SidenavItem getSidenavParent() {
        return parent;
    }

    @Override
    public void setSidenavParent(SidenavItem parent) {
        this.parent = parent;
    }

    @Override
    public SidenavItemType getSidenavItemType() {
        return SidenavItemType.FOLDER;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
