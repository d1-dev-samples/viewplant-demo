package promauto.viewplant.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import promauto.viewplant.auth.AuthService;
import promauto.viewplant.entity.User;
import promauto.viewplant.chart.Chart;
import promauto.viewplant.report.Report;
import promauto.viewplant.sidenav.SidenavFolder;
import promauto.viewplant.sidenav.SidenavItem;
import promauto.viewplant.sidenav.SidenavItemType;
import promauto.viewplant.sidenav.SidenavService;

import java.util.List;
import java.util.stream.Collectors;

import static promauto.viewplant.sidenav.SidenavItemType.*;

@Controller
@RequestMapping("/")
public class AppController {
    Logger logger = LoggerFactory.getLogger(AppController.class);

    private AuthService authService;
    private SidenavService sidenavService;


    public AppController() {
    }

    @Autowired
    public AppController(
            AuthService authService,
            SidenavService sidenavService
    ) {
        this.authService = authService;
        this.sidenavService = sidenavService;
    }

    @GetMapping("login")
    public String login() {
        return "login";
    }


    @GetMapping("welcome")
    public String welcome() {
        return "welcome1";
    }

    @GetMapping("welcome1")
    public String welcome1() {
        return "welcome1";
    }

    @GetMapping("welcome2")
    public String welcome2() {
        return "welcome2";
    }

    @GetMapping("welcome3")
    public String welcome3() {
        return "welcome3";
    }

    @GetMapping("welcome4")
    public String welcome4() {
        return "welcome4";
    }


    @GetMapping({"", "app"})
    public String showMain(Model model) {

        User user = authService.getCurrentUser();

        List<SidenavItem> items = sidenavService.getItems().stream()
                .filter(si -> si.getSidenavItemType() == FOLDER  ||  authService.isPermitted( si.getFullName() ))
                .collect(Collectors.toList());

        items = sidenavService.removeEmptyFolders(items);

        if(items.isEmpty())
            return "login";

        List<SidenavFolder> folders = getItemsOfType(items, FOLDER);
        List<Report>        reports = getItemsOfType(items, REPORT);
        List<Chart>         charts  = getItemsOfType(items, CHART);

        model.addAttribute("folders", folders );
        model.addAttribute("reports", reports );
        model.addAttribute("charts",  charts );

        model.addAttribute("user",    user );

        return "main";
    }

    private <T> List<T> getItemsOfType(List<SidenavItem> items, SidenavItemType type) {
        return items.stream()
                .filter(item -> item.getSidenavItemType() == type)
                .map(item -> (T) item)
                .collect(Collectors.toList());
    }

}
