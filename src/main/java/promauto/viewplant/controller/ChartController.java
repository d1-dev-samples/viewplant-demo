package promauto.viewplant.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import promauto.viewplant.chart.Chart;
import promauto.viewplant.chart.ChartService;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;

@RestController
@RequestMapping("/chart")
public class ChartController {

    private ChartService chartService;

    @Autowired
    public ChartController(ChartService chartService) {
        this.chartService = chartService;
    }


    @GetMapping("/config")
    public Chart getConfig(@RequestParam String name) {

        Chart chart = chartService.findInitChart(name);
        if( chart == null ) {
            // todo: not found
        }

        return chart;
    }

    @GetMapping("/data")
    public void getData(
            @RequestParam String name,
            @RequestParam String dtbeg,
            @RequestParam String dtend,
//            HttpServletRequest request,
            HttpServletResponse response) throws Exception
    {

        // parse dt
        LocalDateTime ldtbeg = null;
        LocalDateTime ldtend = null;
        boolean valid = false;
        try {
            ldtbeg = java.time.LocalDateTime.parse(dtbeg);
            ldtend = java.time.LocalDateTime.parse(dtend);
            valid = ldtbeg.isBefore(ldtend);
        } catch (DateTimeParseException e) {
            // TODO bad dates
            return;
        }

        PrintWriter out = response.getWriter();

        response.setContentType("text/plain; charset=utf-8");

        if( valid ) {
            chartService.getData(name, ldtbeg, ldtend, text -> out.println(text));
        } else {
            // send bad response
        }

    }


}
