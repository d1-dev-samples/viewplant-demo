package promauto.viewplant.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import promauto.viewplant.report.ReportService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Controller
@RequestMapping("/report")
public class ReportController {
    Logger logger = LoggerFactory.getLogger(ReportController.class);

    private ReportService reportService;


    public ReportController() {
    }


    @Autowired
    public ReportController( ReportService reportService ) {
        this.reportService = reportService;
    }


    @GetMapping({"test/{num}"})
    public String showTest(@PathVariable int num, Model model) {
        model.addAttribute("testdiv", "<b>Hello</b>");
        return "report/test" + num;
    }


    @GetMapping({""})
    public String showReportMain() {
        return "report/report-main";
    }


    @GetMapping("/error")
    public String showError() {
        return "report/report-error";
    }


    @GetMapping("/show")
    public void showReport(
            @RequestParam Map<String,String> requestparams,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception
    {
        if( !reportService.show(requestparams, request, response) ) {
            response.sendRedirect("/report/error");
        }
    }


}
