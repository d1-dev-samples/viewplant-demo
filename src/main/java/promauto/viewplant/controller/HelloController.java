package promauto.viewplant.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import promauto.viewplant.auth.UserService;
import promauto.viewplant.databases.DatabaseManager;
import promauto.viewplant.config.ViewplantLoader;
import promauto.viewplant.entity.User;


@Controller
@RequestMapping("/hello")
public class HelloController {

    private UserService userService;
    private DatabaseManager databaseManager;
    private JdbcTemplate jdbcTemplate;


    public HelloController() {
    }

    @Autowired
    public HelloController(
            JdbcTemplate jdbcTemplate,
            UserService userService,
            DatabaseManager databaseManager
        )
    {
        this.jdbcTemplate = jdbcTemplate;
        this.userService = userService;
        this.databaseManager = databaseManager;
    }

    @GetMapping("/info")
    public String showInfo(Model model) {
        User u = userService.getUser("d");
        System.out.println(u);

        String sql = "select * from vp_user where id = 2";
        SqlRowSet rs = jdbcTemplate.queryForRowSet(sql);
        if( rs.next() )
            System.out.println( rs.getString(2) );

        model.addAttribute("props", databaseManager);
        return "hello/info";
    }

    @GetMapping("/users")
    public void showUsers(Model model) {
//        User u = userService.getUser("admin");
//        System.out.println(u);
    }

}
