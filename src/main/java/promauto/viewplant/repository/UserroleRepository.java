package promauto.viewplant.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import promauto.viewplant.entity.Userrole;

import java.util.List;

public interface UserroleRepository extends JpaRepository<Userrole, Integer> {

    List<Userrole> findAllByUserId(int userId);

}
