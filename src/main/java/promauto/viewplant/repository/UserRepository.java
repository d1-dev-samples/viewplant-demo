package promauto.viewplant.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import promauto.viewplant.entity.User;

public interface UserRepository extends JpaRepository<User, Integer> {

    User findByUsername(String username);

}
