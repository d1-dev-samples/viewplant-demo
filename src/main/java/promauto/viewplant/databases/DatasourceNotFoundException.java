package promauto.viewplant.databases;

public class DatasourceNotFoundException extends Throwable {
    private String name;

    public DatasourceNotFoundException(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "DatasourceNotFoundException{" +
                "name='" + name + '\'' +
                '}';
    }
}
