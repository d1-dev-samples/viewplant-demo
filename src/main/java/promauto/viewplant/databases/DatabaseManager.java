package promauto.viewplant.databases;

import java.io.Closeable;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.jdbc.DataSourceBuilder;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import promauto.viewplant.config.ViewplantLoader;

@Component
public class DatabaseManager implements  Closeable {
	Logger logger = LoggerFactory.getLogger(DatabaseManager.class);

	private Map<String, DatabaseConfig> databaseConfigs = new HashMap<>();

	private Map<String, DataSource> dataSources = new HashMap<>();


	public DatabaseManager() {
	}


	@Autowired
	public DatabaseManager(ViewplantLoader viewplantLoader) {
		load( viewplantLoader.getConf() );
	}


	public void load(Map<String, Object> conf) {
		conf = (Map<String, Object>)conf.get("databases");
		if( conf == null )
			return;

		for(String alias: conf.keySet()) {
			Map<String, Object> confProps = (Map<String, Object>) conf.get(alias);
			DatabaseConfig config = new DatabaseConfig();
			loadDatabaseConfig(confProps, config);
			addDatabaseConfig(alias, config);
		}
	}


	private void loadDatabaseConfig(Map<String, Object> conf, DatabaseConfig config) {
		// defaults
		String driver = "org.firebirdsql.jdbc.FBDriver";
		String url = "";
		String username = "SYSDBA";
		String password = "masterkey";
		int maxPoolSize = 10;
		int minIdle = maxPoolSize;

		driver = conf.getOrDefault("driver", driver).toString();
		url = conf.getOrDefault("url", url).toString();
		username = conf.getOrDefault("username", username).toString();
		password = conf.getOrDefault("password", password).toString();
		maxPoolSize = (Integer)conf.getOrDefault("max-pool-size", maxPoolSize);
		minIdle = (Integer)conf.getOrDefault("min-idle", minIdle);

		config.setDriver(driver);
		config.setUrl(url);
		config.setUsername(username);
		config.setPassword(password);
		config.setMaxPoolSize(maxPoolSize);
		config.setMinIdle(minIdle);
	}


	public void addDatabaseConfig(String alias, DatabaseConfig config) {
		databaseConfigs.put(alias, config);
	}


	public DatabaseConfig getDatabaseConfig(String alias) {
		return databaseConfigs.get(alias);
	}


	public List<String> getAliases() {
		List<String> list = databaseConfigs.keySet().stream().collect(Collectors.toList());
		Collections.sort(list);
		return list;
	}


	public synchronized DataSource get(String key) throws DatasourceNotFoundException {
		DataSource ds = dataSources.get(key);
		if( ds == null ) {
			ds = createDataSource(key);
		}

		if( ds == null ) {
			throw new DatasourceNotFoundException(key);
		}
		return ds;
	}


	private DataSource createDataSource(String alias) {
		// get database properties
		DatabaseConfig props = databaseConfigs.get(alias);
		if( props == null )
			return null;


		DataSourceBuilder<?> factory = DataSourceBuilder.create()
				.url(			  props.getUrl()		)
				.username(		  props.getUsername()	)
				.password(		  props.getPassword()	)
				.driverClassName( props.getDriver()		);
		
		DataSource ds = factory.build();

		// init hikari params with the values from the config file
		if( ds instanceof HikariConfig) {
			HikariConfig hds = (HikariConfig)ds;
	        hds.setMaximumPoolSize( props.getMaxPoolSize() );
	        hds.setMinimumIdle( props.getMinIdle() );
		}
		
		dataSources.put(alias, ds);

		return ds;
	}


	@Override
	public void close() {
		logger.debug("DatabaseManager is closign datasources");
		for(DataSource ds: dataSources.values()) {
			HikariDataSource hds = (HikariDataSource)ds;
			hds.close();
		}
	}

}
