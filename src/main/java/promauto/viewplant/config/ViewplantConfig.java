package promauto.viewplant.config;

import net.sf.jasperreports.j2ee.servlets.ImageServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class ViewplantConfig {

//    @Bean
//    public ServletRegistrationBean<ImageServlet> imageServlet() {
//        ServletRegistrationBean<ImageServlet> servRegBean = new ServletRegistrationBean<>();
//        servRegBean.setServlet(new ImageServlet());
//        servRegBean.addUrlMappings("/japser_images");
//        servRegBean.setLoadOnStartup(1);
//        return servRegBean;
//    }

    @Bean
    public ServletRegistrationBean<ImageServlet> imageServletRegistrationBean() {
        ServletRegistrationBean<ImageServlet> servRegBean = new ServletRegistrationBean<>();
        servRegBean.setServlet(new ImageServlet());
        servRegBean.addUrlMappings("/report/images");
        servRegBean.setLoadOnStartup(1);
        return servRegBean;
    }



    // disable CORS while debugging
//    @Bean
//    public WebMvcConfigurer corsConfigurer() {
//        return new WebMvcConfigurer() {
//            @Override
//            public void addCorsMappings(CorsRegistry registry) {
//                registry.addMapping("/**")
//                        .allowedMethods("HEAD", "GET", "PUT", "POST", "DELETE", "PATCH");
//            }
//        };
//    }

}
