package promauto.viewplant.config;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import org.yaml.snakeyaml.Yaml;

@Component
public class ViewplantLoader {
	Logger logger = LoggerFactory.getLogger(ViewplantLoader.class);


	private final String filename;

	private Map<String, Object> conf;


	@Autowired
	public ViewplantLoader(Environment env){

		Yaml yaml = new Yaml();
		filename = env.getProperty("viewplant.config.file");
		Path path = Paths.get(filename);
		if( Files.exists(path) ) {
			logger.info("Loading from properties: " + path.toString());
			try (InputStream in = Files.newInputStream(path)) {
				conf = yaml.load(in);
			} catch (Exception e) {
				logger.error("Properties loading error", e);
			}
		} else {
			ClassPathResource resource = new ClassPathResource(filename);
			if( resource.exists() ) {
				try (InputStream in = resource.getInputStream()) {
					conf = yaml.load(in);
				} catch (Exception e) {
					logger.error("Properties loading error", e);
				}
			} else {
				logger.error("Properties file is not found: viewplant.properties.file=" + filename);
			}
		}


		if( conf == null )
			conf = new HashMap<>();
	}


	public Map<String, Object> getConf() {
		return conf;
	}


}
