package promauto.viewplant.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import promauto.viewplant.databases.DatabaseManager;
import promauto.viewplant.databases.DatasourceNotFoundException;

import javax.sql.DataSource;

@Configuration
public class PrimaryDataSourceConfig {

    @Bean(name = "dsPrimary")
    @Autowired
    public DataSource dataSource(DatabaseManager databaseManager) throws DatasourceNotFoundException {
        return databaseManager.get("primary");
    }


//    @Bean(name = "jdbcPrimary")
    @Bean
    @Autowired
    public JdbcTemplate jdbcTemplate(@Qualifier("dsPrimary") DataSource dsPrimary) {
        return new JdbcTemplate(dsPrimary);
    }



}