package promauto.viewplant.chart;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ChartSerie {
    private String name;

    private int id;         // can be requested
    private String descr;   // by name

    private String color;
    private Double width;

    @JsonIgnore
    private String sqlInit = "";

    @JsonIgnore
    private String sqlData = "";



    public ChartSerie() {

    }

    public ChartSerie(String name, int id, String descr, String color, Double width) {
        this.name = name;
        this.id = id;
        this.descr = descr;
        this.color = color;
        this.width = width;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public String getSqlInit() {
        return sqlInit;
    }

    public void setSqlInit(String sqlInit) {
        this.sqlInit = sqlInit;
    }

    public String getSqlData() {
        return sqlData;
    }

    public void setSqlData(String sqlData) {
        this.sqlData = sqlData;
    }
}

