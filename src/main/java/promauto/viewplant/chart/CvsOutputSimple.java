package promauto.viewplant.chart;

import java.time.LocalDateTime;
import java.util.function.Consumer;

public class CvsOutputSimple extends CvsOutput {


    public CvsOutputSimple(int precision, Consumer<String> output) {
        this.output = output;
        fmtVal.setMaximumFractionDigits(precision);
    }


    @Override
    public void push(int id, LocalDateTime dt, double val) {
        write(id, dt, val);
    }


}
