package promauto.viewplant.chart;

import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.function.Consumer;

public abstract class CvsOutput {
    protected static final String COLUMNS = "id,dt,val";

    protected DateTimeFormatter fmtDtShort = DateTimeFormatter.ofPattern("yyMMddHHmmss");
    protected DecimalFormat fmtVal = new DecimalFormat("#");
    protected Consumer<String> output;

    public void start() {
        output.accept(COLUMNS);
    }

    public void write(int id, LocalDateTime dt, double val) {
        output.accept( id + "," + dt.format(fmtDtShort) + "," + fmtVal.format(val));
    }

    abstract  public void push(int id, LocalDateTime dt, double val);

    public void finish() {}



}
