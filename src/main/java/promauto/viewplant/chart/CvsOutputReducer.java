package promauto.viewplant.chart;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.function.Consumer;

public class CvsOutputReducer extends CvsOutput {

    private int curid;
    private double minVal;
    private double maxVal;
    private LocalDateTime minDt;
    private LocalDateTime maxDt;

    private double lastVal;
    private LocalDateTime lastDt;

    private LocalDateTime perEnd;
    private long period;



    public CvsOutputReducer(LocalDateTime dtbeg, LocalDateTime dtend, int density, int precision, Consumer<String> output) {
        fmtVal.setMaximumFractionDigits(precision);

        this.output = output;

        int n = (density - 2) / 2;
        period = dtbeg.until(dtend, ChronoUnit.NANOS) / n + 1;

        curid = 0;
        startPeriod(dtbeg);
    }


    @Override
    public void push(int id, LocalDateTime dt, double val) {

        if( curid != id) {
            writeMinMaxLast();

            curid = id;
            startPeriod(dt);

            write(curid, dt, val);
            return;
        }

        if( dt.isAfter(perEnd) ) {
            writeMinMax();
            startPeriod(dt);
        }

        if( minDt == null  ||  val < minVal) {
            minDt = dt;
            minVal = val;
        }

        if( maxDt == null  ||  val > maxVal) {
            maxDt = dt;
            maxVal = val;
        }

        lastDt = dt;
        lastVal = val;
    }


    private void startPeriod(LocalDateTime dt) {
        perEnd = dt.plus(period, ChronoUnit.NANOS);
        minDt = maxDt = lastDt = null;
    }


    private void writeMinMax() {
        if( minDt == null  &&  maxDt == null)
            return;

        if( minDt != null  &&  maxDt == null )
            write(curid, minDt, minVal);
        else

        if( minDt == null  &&  maxDt != null )
            write(curid, maxDt, maxVal);
        else

        if( minDt.equals(maxDt) )
            write(curid, minDt, minVal);
        else

        if( minDt.isBefore(maxDt) ) {
            write(curid, minDt, minVal);
            write(curid, maxDt, maxVal);
        } else {
            write(curid, maxDt, maxVal);
            write(curid, minDt, minVal);
        }
    }


    private void writeMinMaxLast() {
        writeMinMax();

        if( lastDt != null
                &&  (minDt==null  ||  !minDt.equals(lastDt))
                &&  (maxDt==null  ||  !maxDt.equals(lastDt))  )
            write(curid, lastDt, lastVal);
    }



    @Override
    public void finish() {
        writeMinMaxLast();
    }
}
