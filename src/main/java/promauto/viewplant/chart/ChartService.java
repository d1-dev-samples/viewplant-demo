package promauto.viewplant.chart;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import promauto.viewplant.config.ViewplantLoader;
import promauto.viewplant.util.MapObjectFacade;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Service
public class ChartService {
    Logger logger = LoggerFactory.getLogger(ChartService.class);

    private ChartDAO chartDAO;

    private Map<String, Chart> charts = new HashMap<>();


    @Autowired
    public ChartService(ViewplantLoader viewplantLoader, ChartDAO chartDAO) {
        this.chartDAO = chartDAO;
        load( viewplantLoader.getConf() );
    }


    public void load(Map<String, Object> conf) {
        MapObjectFacade m = new MapObjectFacade(conf.get("charts"));
        m = new MapObjectFacade(m.get("chart.items"));

        for (String name : m.keySet()) {
            Chart chart = new Chart(name);
            charts.put(name, chart);

            MapObjectFacade mm = new MapObjectFacade(m.get(name));

            chart.setDatabase(mm.get("database", ""));
            chart.setTitle(mm.get("title", name));
            chart.setUnits(mm.get("units", ""));
            chart.setCaption(mm.get("caption", name));
            chart.setPrecision(mm.get("precision", 0));
            chart.setValmin(mm.get("valmin", 0.0));
            chart.setValmax(mm.get("valmax", 0.0));
            chart.setPeriod(mm.get("period", 0));
            chart.setDensity(mm.get("density", 0));
            chart.setSqlInit(mm.get("sqlConfig", ""));
            chart.setSqlData(mm.get("sqlData", ""));

            for (Object obj : mm.getList("series")) {
                ChartSerie serie = new ChartSerie();
                chart.addSerie(serie);

                mm = new MapObjectFacade(obj);
                serie.setId(mm.get("id", 0));
                serie.setName(mm.get("name", ""));
                serie.setDescr(mm.get("descr", ""));
                serie.setColor(mm.get("color", ""));
                serie.setWidth(mm.get("width", 0.0));
                serie.setSqlInit(mm.get("sqlConfig", ""));
                serie.setSqlData(mm.get("sqlData", ""));
            }
        }
    }


    private synchronized void initChart(Chart chart) {
        if (chart.isInitialized())
            return;

        if (chart.hasEmptySeries()) {
            chart.prepareSqlInit();
            chartDAO.initChart(chart);
            chart.prepareSqlData();
        }

        chart.setInitialized(true);
    }


    public Chart getChart(String name) {
        return charts.get(name);
    }

    public Chart findInitChart(String name) {
        Chart chart = charts.get(name);

        if (chart != null && !chart.isInitialized())
            initChart(chart);

        return chart;
    }


    public void getData(String name, LocalDateTime dtbeg, LocalDateTime dtend, Consumer<String> output) {
        Chart chart = charts.get(name);
        if (chart == null)
            return;

        if (!chart.isInitialized())
            initChart(chart);


        CvsOutput cvsOutput;
        if( chart.getDensity() > 0)
            cvsOutput = new CvsOutputReducer(dtbeg, dtend, chart.getDensity(), chart.getPrecision(), output);
        else
            cvsOutput = new CvsOutputSimple(chart.getPrecision(), output);


        chartDAO.getData(chart, dtbeg, dtend, cvsOutput);

    }

}