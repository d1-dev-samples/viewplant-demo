package promauto.viewplant.chart;

import com.fasterxml.jackson.annotation.JsonIgnore;
import promauto.viewplant.sidenav.SidenavItem;
import promauto.viewplant.sidenav.SidenavItemType;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Chart implements SidenavItem {
//implements Serializable

    private int sidenavId;
    private SidenavItem sidenavParent = null;

    private String name;
    private String title;
    private String units;
    private String caption;

    private int    precision;
    private double valmin;
    private double valmax;
    private int    period;
    private int    density;

    private List<ChartSerie> series = new ArrayList<>();

    @JsonIgnore
    private String database;

    @JsonIgnore
    private String sqlInit = "";
    @JsonIgnore
    private String sqlData = "";
    @JsonIgnore
    private boolean initialized = false;




    public Chart(String name) {
        this.name = name;
    }

    public Chart(String name, String title, String units, double valmin, double valmax, int period) {
        this.name = name;
        this.title = title;
        this.units = units;
        this.valmin = valmin;
        this.valmax = valmax;
        this.period = period;
    }

    // auto-generated getters and setters

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public int getPrecision() {
        return precision;
    }

    public void setPrecision(int precision) {
        this.precision = precision;
    }

    public double getValmin() {
        return valmin;
    }

    public void setValmin(double valmin) {
        this.valmin = valmin;
    }

    public double getValmax() {
        return valmax;
    }

    public void setValmax(double valmax) {
        this.valmax = valmax;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public int getDensity() {
        return density;
    }

    public void setDensity(int density) {
        this.density = density;
    }

    public String getSqlInit() {
        return sqlInit;
    }

    public void setSqlInit(String sqlInit) {
        this.sqlInit = sqlInit;
    }

    public String getSqlData() {
        return sqlData;
    }

    public void setSqlData(String sqlData) {
        this.sqlData = sqlData;
    }

    public List<ChartSerie> getSeries() {
        return series;
    }

    public void setSeries(List<ChartSerie> series) {
        this.series = series;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public boolean isInitialized() {
        return initialized;
    }

    public void setInitialized(boolean initialized) {
        this.initialized = initialized;
    }



    // additional getters and setters
    public void addSerie(ChartSerie serie) {
        series.add(serie);
    }






    public boolean hasEmptySeries() {
        return series.stream()
                .anyMatch( ser -> ser.getId() <= 0  ||  ser.getDescr().isEmpty() );
    }


    public String joinIds() {
        return series.stream()
                .filter(ser -> ser.getId() > 0)
                .map(ser -> "" + ser.getId() )
                .collect(Collectors.joining(","));
    }


    public String joinNames() {
        return series.stream()
                .filter(ser -> !ser.getName().isEmpty())
                .map(ser -> "'" + ser.getName() + "'")
                .collect(Collectors.joining(","));
    }



    public void prepareSqlInit() {
        sqlInit = sqlInit
                .replace("{names}", joinNames() )
                .replace("{ids}", joinIds() );


        String sql = series.stream()
                .filter(ser -> !ser.getSqlInit().isEmpty())
                .map(ser -> ser.getSqlInit()
                        .replace("{name}", "'"+ser.getName()+"'")
                        .replace("{id}", "" + ser.getId()))
                .collect(Collectors.joining(" union "));

        sqlInit += (sqlInit.isEmpty() || sql.isEmpty()? "": " union ") + sql;
    }


    public void prepareSqlData() {
        sqlData = sqlData
                .replace("{names}", joinNames() )
                .replace("{ids}", joinIds() );


        String sql = series.stream()
                .filter(ser -> !ser.getSqlData().isEmpty())
                .map(ser -> ser.getSqlData()
                        .replace("{name}", "'"+ser.getName()+"'")
                        .replace("{id}", "" + ser.getId()))
                .collect(Collectors.joining(" union "));

        sqlData += (sqlData.isEmpty() || sql.isEmpty()? "": " union ") + sql + " order by 1,2";
    }

    @Override
    public int getSidenavId() {
        return sidenavId;
    }

    @Override
    public void setSavenavId(int id) {
        sidenavId = id;
    }

    @Override
    public SidenavItem getSidenavParent() {
        return sidenavParent;
    }

    @Override
    public void setSidenavParent(SidenavItem parent) {
        sidenavParent = parent;
    }

    @Override
    public SidenavItemType getSidenavItemType() {
        return SidenavItemType.CHART;
    }


}


