package promauto.viewplant.chart;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import promauto.viewplant.databases.DatabaseManager;
import promauto.viewplant.databases.DatasourceNotFoundException;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class ChartDAO {
    Logger logger = LoggerFactory.getLogger(ChartDAO.class);

    private DatabaseManager databaseManager;

    @Autowired
    public ChartDAO(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
    }

    public void initChart(Chart chart) {

        try( Connection conn = databaseManager.get(chart.getDatabase()).getConnection() ) {

            Map<Integer,ChartSerie> mapSerieId = chart.getSeries().stream()
                    .filter(ser -> ser.getId() > 0)
                    .collect(Collectors.toMap(ChartSerie::getId, Function.identity()));

            Map<String,ChartSerie> mapSerieName = chart.getSeries().stream()
                    .filter(ser -> !ser.getName().isEmpty())
                    .collect(Collectors.toMap(ChartSerie::getName, Function.identity()));

            Statement st = conn.createStatement();
            try( ResultSet rs = st.executeQuery(chart.getSqlInit()) ) {
                while (rs.next()) {
                    int id = rs.getInt(1);
                    String name = rs.getString(2);
                    String descr = rs.getString(3);

                    ChartSerie ser;
                    if( (ser = mapSerieId.get(id)) != null  ||  (ser = mapSerieName.get(name)) != null ) {
                        if( ser.getId() <= 0)
                            ser.setId(id);

                        if( ser.getName().isEmpty() )
                            ser.setName(name);

                        if( ser.getDescr().isEmpty() )
                            ser.setDescr(descr);
                    }
                }
            }

            chart.getSeries().stream()
                    .filter(ser -> ser.getDescr().isEmpty())
                    .forEach(ser -> ser.setDescr( ser.getName() ));

        } catch (SQLException | DatasourceNotFoundException e) {
            logger.error("Chart init error", e);
        } catch (IllegalStateException e) {
            logger.error("Found duplicated id/name", e);
        }

    }



    public static final DateTimeFormatter fmtDt = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public void getData(Chart chart, LocalDateTime dtbeg, LocalDateTime dtend, CvsOutput cvsOutput) {
        try( Connection conn = databaseManager.get(chart.getDatabase()).getConnection() ) {

            String sql = chart.getSqlData()
                    .replace("{dtbeg}", "'" + dtbeg.format(fmtDt) + "'" )
                    .replace("{dtend}", "'" + dtend.format(fmtDt) + "'" );


            Statement st = conn.createStatement();
            try( ResultSet rs = st.executeQuery(sql) ) {

                cvsOutput.start();

                while (rs.next())
                    cvsOutput.push(
                        rs.getInt(1),
                        rs.getTimestamp(2).toLocalDateTime(),
                        rs.getDouble(3) );

                cvsOutput.finish();
            }
        } catch (SQLException | DatasourceNotFoundException e) {
            logger.error("Chart getData error", e);
        }
    }
}
