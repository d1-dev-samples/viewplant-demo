package promauto.viewplant.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/*
CREATE SEQUENCE SQ_VP_USERROLE_ID;

CREATE TABLE VP_USERROLE (
  ID integer not null constraint PK_VP_USERROLE primary key,
  USER_ID integer not null,
  ROLENAME varchar(128) not null,
  constraint FK_VP_USERROLE_ID foreign key (USER_ID) references VP_USER(ID) on update cascade on delete cascade
);

CREATE OR ALTER trigger VP_USERROLE_BI for VP_USERROLE
active before insert position 0
AS
BEGIN
  IF (NEW.ID IS NULL) THEN
    NEW.ID = GEN_ID(SQ_VP_USERROLE_ID,1);
END
*/


@Entity
@Table(name="vp_userrole")
public class Userrole {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY, generator = "sq_vp_userrole_id")
    @SequenceGenerator(name = "sq_vp_userrole_id", sequenceName = "sq_vp_userrole_id", allocationSize = 1)
    @Column(name="id")
    Integer id;

    @Column(name="user_id")
    Integer userId;

    @Column(name="rolename")
    String rolename;


    public Userrole() {
    }

    public Userrole(Integer userId, String rolename) {
        this.userId = userId;
        this.rolename = rolename;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getRolename() {
        return rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }


    @Override
    public String toString() {
        return "Userrole{" +
                "id=" + id +
                ", userId='" + userId + '\'' +
                ", rolename='" + rolename + '\'' +
                '}';
    }
}
