package promauto.viewplant.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/*
CREATE SEQUENCE SQ_VP_USER_ID;

CREATE TABLE VP_USER (
  ID integer not null constraint PK_VP_USER primary key,
  USERNAME varchar(64) constraint UQ_VP_USER_USERNAME unique,
  FULLNAME varchar(128),
  PWD varchar(64)

)

CREATE OR ALTER trigger VP_USER_BI for VP_USER
active before insert position 0
AS
BEGIN
  IF (NEW.ID IS NULL) THEN
    NEW.ID = GEN_ID(SQ_VP_USER_ID,1);
END

*/


@Entity
@Table(name="vp_user")
public class User {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY, generator = "sq_vp_user_id")
    @SequenceGenerator(name = "sq_vp_user_id", sequenceName = "sq_vp_user_id", allocationSize = 1)
    @Column(name="id")
    Integer id;

    @Column(name="username")
    String username;

    @Column(name="fullname")
    String fullname;

    @Column(name="pwd")
    String password;


    public User() {
    }

    public User(Integer id, String username, String fullname, String password) {
        this.id = id;
        this.username = username;
        this.fullname = fullname;
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public List<String> getRolenames() {
        return new ArrayList<>();
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", fullname='" + fullname + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
