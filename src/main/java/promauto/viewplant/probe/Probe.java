package promauto.viewplant.probe;


import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class Probe {

/*
Есть строка, состоящая из слов. Все слова в ней разделены одним пробелом.
Нужно преобразовать строку в такую структуру данных, которая группирует
слова по первой букве в слове. Затем вывести только группы, содержащие
более одного элемента.

Группы должны быть отсортированы в алфавитном порядке. Слова внутри группы
нужно сортировать по убыванию количества символов; если количество символов
равное, то сортировать в алфавитном порядке.

Пример строки: String s = «сапог сарай арбуз болт бокс биржа»

Отсортированная строка: [б=[биржа, бокс, болт], c=[caпог, сарай]]

code started at 00:45
code stopped at 01:11
*/

    public static void parseAndPrint(String src) {

        // проверка на пустую строку на входе
        if(src.trim().isEmpty())
            return;

        // инициализация структуры данных
        Map<Character, ArrayList<String>> res = new TreeMap<>();

        // парсинг исходной строки и заполнение результирующей структуры данных
        String[] words = src.split(" ");
        for(String word: words) {
            Character ch = word.charAt(0);
            ArrayList<String> arr = res.get(ch);
            if( arr == null ) {
                arr = new ArrayList<>();
                res.put(ch, arr);
            }
            arr.add(word);
        }

        // сортировка внутри групп
        res.values().forEach(arr -> arr.sort( (s1, s2) -> {
            return s1.length() != s2.length()?
                    s2.length() - s1.length() : // сначала по убыванию количества символов
                    s1.compareTo(s2);           // затем в алфавитном порядке
        }));

        // преобразование структуры данных в строку
        String dst = res.entrySet().stream()
                .filter(ent -> ent.getValue().size() > 1)
                .map(ent -> ent.getKey() + "=[" + String.join(", ", ent.getValue()) + "]" )
                .collect(Collectors.joining(", "));

        // вывод
        System.out.println(dst);
    }

    public static void main(String[] args) {
        parseAndPrint("сапог сарай арбуз болт бокс биржа");
    }

/*
Результат выполнения:

б=[биржа, бокс, болт], с=[сапог, сарай]

Process finished with exit code 0
*/


}
