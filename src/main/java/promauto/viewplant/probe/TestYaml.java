package promauto.viewplant.probe;

import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;
import org.yaml.snakeyaml.nodes.Tag;
import promauto.viewplant.chart.Chart;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;

public class TestYaml {



    public static void main(String[] args) {


        String filename = "viewplant.yml";

        ClassPathResource cpr = new ClassPathResource(filename);
        System.out.println( cpr.exists() );

        if( false ) {
            Yaml yaml = new Yaml();
            try (InputStream in = cpr.getInputStream()) {

                Map<String, Object> map = yaml.load(in);
//            System.out.println(map.size());
//            System.out.println(map);

                Object obj = map.get("charts");
                map = (Map<String, Object>) obj;

                obj = map.get("chart.items");
                map = (Map<String, Object>) obj;

                for (String key : map.keySet()) {
                    obj = map.get(key);
                    String conf = yaml.dump(obj);
                    System.out.println(conf);


                    Yaml yaml1 = new Yaml(new Constructor(Chart.class));
                    Chart chart = yaml.load(conf);
                    System.out.println(chart);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

/*
        Yaml yaml = new Yaml();
        Chart chart = new Chart("line", "aaa", "titleee", "unitsss", 0, 1.5, 1000);

        String conf = yaml.dumpAs(chart, Tag.MAP, null);
        System.out.println(conf);

        Yaml yaml1 = new Yaml(new Constructor(Chart.class));
        Chart chart1 = yaml.load(conf);
        System.out.println(chart1);
*/


    }
}
