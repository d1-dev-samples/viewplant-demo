package promauto.viewplant.probe;

import org.yaml.snakeyaml.Yaml;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class TestH2 {
    public static void main(String[] args) {


        Yaml yaml = new Yaml();

        System.out.println("Test H2 connection...");


        String url = "jdbc:firebirdsql://localhost:3050/hbcustomer";

//        Properties props = new Properties();
//        props.put("user", "SYSDBA");
//        props.put("password", "masterkey");
//        props.put("encoding", "WIN1251");


        try {

//            Class.forName("org.firebirdsql.jdbc.FBDriver");
//            Connection conn = DriverManager.getConnection(url, props);

            Class.forName("org.h2.Driver");
            Connection conn = DriverManager.getConnection("jdbc:h2:~/test", "sa", "");

            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
}
