package promauto.viewplant;

import net.sf.jasperreports.j2ee.servlets.ImageServlet;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

/*
@SpringBootApplication
public class ViewplantApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(ViewplantApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(ViewplantApplication.class);
    }
}
*/


@SpringBootApplication
public class ViewplantApplication {

    public static void main(String[] args) {
        SpringApplication.run(ViewplantApplication.class, args);
    }
}


//    @Bean
//    public ServletRegistrationBean<ImageServlet> imageServletRegistrationBean() {
//        ServletRegistrationBean<ImageServlet> servRegBean = new ServletRegistrationBean<>();
//        servRegBean.setServlet(new ImageServlet());
//        servRegBean.addUrlMappings("/report/images");
//        servRegBean.setLoadOnStartup(1);
//        return servRegBean;
//    }
//



