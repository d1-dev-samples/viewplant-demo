function createLineChart(parent, theConfigUrl, theDataUrl) {

// ----- consts and vars -----
    const mobile = window.isMobile===undefined? false: isMobile.any;
    if( mobile )
        console.log("Modile mode");

    let urlConfig = theConfigUrl;
    let urlData = theDataUrl;

    let width = 900;
    let height = 300;
    let adj = 30;
    const adjLeft = 60;
    const adjTop = 25;
    const adjRight = 30;
    const adjBottom = 45;
    const adjLegendBottom = 17;

    const transitDuration = 500;
    let zoomDir = "xy";

    let dtbeg, dtend, valmin, valmax;

    let chart = {
        series: []
    };

    let xHoverVisible;
    let yHoverVisible;
    let hoverX, hoverY;
    let hoverVal, hoverDt;



// ----- formatting -----

    const dtConvT = d3.timeParse("%Y-%m-%dT%H:%M:%S");
    const dtConv = d3.timeParse("%y%m%d%H%M%S");
    const dateConv = d3.timeParse("%Y-%m-%d");

    d3.timeFormatDefaultLocale({
        'dateTime': '%A, %e %B %Y г. %X',
        'date': '%d.%m.%Y',
        'time': '%H:%M:%S',
        'periods': ['AM', 'PM'],
        'days': ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
        'shortDays': ['вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб'],
        'months': ['январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь'],
        'shortMonths': ['янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек']
    });


    const
        formatMillisecond = d3.timeFormat(".%L"),
        formatSecond = d3.timeFormat(":%S"),
        formatMinute = d3.timeFormat("%H:%M"),
        formatHour = d3.timeFormat("%H"),
        formatDay = d3.timeFormat("%a %d"),
        formatWeek = d3.timeFormat("%b %d"),
        formatMonth = d3.timeFormat("%B"),
        formatYear = d3.timeFormat("%Y");

    const
        // formatDt = d3.timeFormat("%d.%m.%Y %H:%M:%S"),
        formatDtT = d3.timeFormat("%Y-%m-%dT%H:%M:%S"),
        formatDt = d3.timeFormat("%d %B, %Y, %H:%M:%S");
    let formatVal = d3.format(",.3r");


    function multiFormat(date) {
        return (
            d3.timeSecond(date) < date ? formatMillisecond
                : d3.timeMinute(date) < date ? formatSecond
                : d3.timeHour(date) < date ? formatMinute
                    : d3.timeDay(date) < date ? formatHour
                        : d3.timeMonth(date) < date ? (d3.timeWeek(date) < date ? formatDay : formatWeek)
                            : d3.timeYear(date) < date ? formatMonth
                                : formatYear)(date);
    }

    let colorList = [
        "red", "green", "blue","gray", "purple", "yellow", "lime",
        "#FF00FF",
        "#800000",
        "#C0DCC0",
        "#000080",
        "#808000",
        "#87CEEB",
        "#008080"
    ];


// ----- scales -----
    const xScale = d3.scaleTime()
    // .range([0, width]);

    const yScale = d3.scaleLinear().rangeRound([height, 0]);

// ----- axis ------
    const yAxis = d3.axisLeft();
    const xAxis = d3.axisBottom()
        .tickFormat(multiFormat);

// ----- grid lines ------
    const xGrid = d3.axisBottom()
        .tickSize(-height)
        .tickFormat("");

    const yGrid = d3.axisLeft()
    // .tickSize(-width)
        .tickFormat("");

// ----- serie lines ------
    const line = d3.line()
        .x(function (d) {
            return xScale(d.dt);
        })
        .y(function (d) {
            return yScale(d.val);
        })
        // .curve(d3.curveLinear) // good
        // .curve(d3.curveMonotoneX) // good
        .curve(d3.curveStepAfter); // good



    const parentSel = d3.select(parent);

    const svg = parentSel.select(".lc-container").append("svg")
        .attr("preserveAspectRatio", "xMinYMin meet");
        // .style("padding", padding)
        // .style("margin", margin);

    const xGridG = svg.append("g")
        .attr("class", "lc-grid")

    const yGridG = svg.append("g")
        .attr("class", "lc-grid");


    const xAxisG = svg.append("g")
        .attr("class", "lc-axis")

    const yAxisG = svg.append("g")
        .attr("class", "lc-axis");

    yAxisG
        .append("text")
        .attr("class", "lc-y-axis-units")
        .attr("transform", "rotate(-90)")
        .attr("dy", ".75em")
        .attr("y", 6)
        .style("text-anchor", "end");

    // const statusLabel = svg.append("text")
    //     .attr("class", "lc-status")
    //     .style("text-anchor", "end");

    const statusSel = parentSel.select(".lc-status");


    svg.append("defs").append("svg:clipPath")
        .attr("id", "linechart-clip")
        .append("svg:rect")
        .attr("x", 0)
        .attr("y", 0);

    const context = svg.append("g");

    const xHoverLine = context.append("line")
        .attr("class", "lc-hoverline");

    const yHoverLine = context.append("line")
        .attr("class", "lc-hoverline");


    const xLableHeight = 24;
    const yLableHeight = 20;

    const xLabelG = context.append("g");

    const xLabelRect = xLabelG.append("rect")
        .attr("class", "lc-hoverlabel-rect")
        .attr("transform", "translate(0,1)")
        .attr("height", xLableHeight);


    const xLabel = xLabelG.append("text")
        .attr("class", "lc-hoverlabel-x")
        .attr("dy", 18)
        .style("text-anchor", "middle");


    const yLabelG = context.append("g");

    yLabelG.append("rect")
        .attr("class", "lc-hoverlabel-rect")
        .attr("x", -adjLeft)
        .attr("y", -yLableHeight/2)
        .attr("width", adjLeft)
        .attr("height", yLableHeight);
        // .attr("fill", "#636363");

    const yLabel = yLabelG.append("text")
        .attr("class", "lc-hoverlabel-y")
        .attr("x", -9)
        .attr("y", 5)
        .style("text-anchor", "end");


    setHoverVisible(true);
    setHoverVisible(false);


    const titleSel = parentSel.select(".lc-title");


    const legendTable = parentSel.select(".lc-legend").append("table");

    const inputGotoDt= parentSel.select(".lc-goto-dt")
            .on("change", () => goto(inputGotoDt.value + "T12:00:00"))
            .node();

    parentSel.select(".lc-zoom-toggle").on("click", function(){lc.toggleZoomDir(this);});
    parentSel.select(".lc-zoom-res")   .on("click", () => lc.resetZoom());
    parentSel.select(".lc-zoom-in")    .on("click", () => lc.zoomIn());
    parentSel.select(".lc-zoom-out")   .on("click", () => lc.zoomOut());
    parentSel.select(".lc-move-left")  .on("click", () => lc.move(-1, 0));
    parentSel.select(".lc-move-up")    .on("click", () => lc.move(0, 1));
    parentSel.select(".lc-move-down")  .on("click", () => lc.move(0, -1));
    parentSel.select(".lc-move-right") .on("click", () => lc.move(1, 0));
    parentSel.select(".lc-period-1d")  .on("click", () => lc.setPeriod(86400));
    parentSel.select(".lc-period-8h")  .on("click", () => lc.setPeriod(28800));
    parentSel.select(".lc-period-1h")  .on("click", () => lc.setPeriod(3600));
    parentSel.select(".lc-period-10m") .on("click", () => lc.setPeriod(600));

// ----- init end -----





    function resize(newWidth, newHeight) {
        width = newWidth;
        height = newHeight;

        xScale.range([0, width]);
        yScale.rangeRound([height, 0]);

        xGrid.tickSize(-height);

        yGrid.tickSize(-width);

        svg.attr("viewBox", `-${adjLeft} -${adjTop} ${width + adjLeft + adjRight} ${height + adjTop + adjBottom}`);

        xGridG.attr("transform", "translate(0," + height + ")");

        xAxisG.attr("transform", "translate(0," + height + ")");

        svg.select("#linechart-clip rect")
            .attr("width", width)
            .attr("height", height);

        xHoverLine.attr("y2", height);

        yHoverLine.attr("x2", width);

        // xLabel.attr("dy", (height + 35) + "px");
        xLabel.attr("y", height);

        xLabelRect
            .attr("y", height);

        let glines = svg.selectAll(".lc-line")
            .data(chart.series)
            .select("path")
            .attr("d", ser => line(ser.data));

        setBrush();
        setScales();
        scaleAxises();
    }


    function adjustSize() {
        let cont = svg.node().parentNode;
        let lgnd = legendTable.node();

        let w;
        let h;

        if (mobile) {
            cont.style.width = window.outerWidth + "px";
            w = window.outerWidth - adjLeft - adjRight;
            h = window.outerHeight  - cont.offsetTop - adjTop - adjBottom;
        } else {
            w = cont.clientWidth - adjLeft - adjRight;

            const svgHeight = window.innerHeight - cont.offsetTop;
            svg.attr('height', svgHeight + 'px');

            h = svgHeight - adjTop - adjBottom;
        }

        console.log(`adjusting size: ${w} x ${h}`);

        if( w > 0  &&  h > 0) {
            setHoverVisible(false);
            resize(w, h);
        }
    }


//----------------------------------------------------------------------------------------------
    let eventsDisabled = true;

    function installEvents() {
        svg.on('mousemove', function () {
            updateHovers();
            updateHoverValues();
            // updateLegendValues();

            drag();
        })
            .on("touchstart", () => {
                onTouchStart();
            })
            .on("touchend", () => {
                onTouchEnd();
            })
            .on("touchmove", () => {
                onTouchMove();
            })
            .on("mousedown", () => {
                if (d3.event.button === 1)
                    startDrag();
            })
            .on("mouseup", () => {
                if (d3.event.button === 1)
                    finishDrag();
            })
            .on("mouseleave", () => {
                setHoverVisible(false)
            });

        context.select(".overlay")
            .attr("pointer-events", "all");

        eventsDisabled = false;
    }

    function removeEvents() {
        svg
            .on("touchstart", null)
            .on("touchend", null)
            .on("mousemove", null)
            .on("mousedown", null)
            .on("mouseup", null)
            .on("mouseleave", null);

        context.select(".overlay")
            .attr("pointer-events", "none");

        eventsDisabled = true;
    }


    let longTouchTimer;
    function resetLongTouchTimer() {
        if (longTouchTimer) {
            clearTimeout(longTouchTimer);
            longTouchTimer = null;
        }
    }

    function onTouchStart() {
        longTouchTimer = setTimeout(moveToHovers, 500)
        updateHovers();
    }

    function onTouchEnd() {
        resetLongTouchTimer();
    }

    function onTouchMove() {
        resetLongTouchTimer();
    }


    function setScales() {
        if( !dtbeg  ||  !dtend ) return;

        xScale.domain([dtbeg, dtend]);
        yScale.domain([valmin, valmax]);
        if (inputGotoDt != null) {
            let half = (dtend.getTime() - dtbeg.getTime()) / 2;
            let dt = new Date();
            dt.setTime(dtbeg.getTime() + half);
            inputGotoDt.value = dt.toISOString().slice(0, 10);
        }

        formatVal = d3.format( yScale.tickFormat() );
    }


    function scaleAxises(useTransition = false) {
        xGrid.scale(xScale);
        yGrid.scale(yScale);
        xAxis.scale(xScale);
        yAxis.scale(yScale);

        if (useTransition) {
            xGridG.transition().duration(transitDuration).call(xGrid);
            yGridG.transition().duration(transitDuration).call(yGrid);
            xAxisG.transition().duration(transitDuration).call(xAxis);
            yAxisG.transition().duration(transitDuration).call(yAxis);
        } else {
            xGridG.call(xGrid);
            yGridG.call(yGrid);
            xAxisG.call(xAxis);
            yAxisG.call(yAxis);
        }
    }


// ===== legend procs =====

    function drawLegend() {
        if (!legendTable) return;

        let rows = legendTable.selectAll("tr")
            .data(chart.series)
            .enter()
            .append("tr");

        rows.append("td")
            .attr("class", "lc-legend-color")
            .append("span");

        rows.append("td")
            .attr("class", "lc-legend-descr");

        legendTable.selectAll("tr")
            .data(chart.series)
            .exit().remove();

        legendTable.selectAll("tr")
            .data(chart.series)
            .attr("class", ser => "lc-legend-" + ser.id)
            .on("click", toggleLineVisible);


        chart.series.forEach(ser => {
            let row = legendTable.select(".lc-legend-" + ser.id);
            row.select(".lc-legend-color span")
                .style("background-color", ser.color);

            row.select(".lc-legend-descr").text(ser.descr);
        });
    }

    function toggleLineVisible(ser) {
        if (eventsDisabled) return;

        let hidden = ser.hidden = !ser.hidden;

        legendTable.select(".lc-legend-" + ser.id)
            .classed("lc-legend-disabled", hidden);

        svg.select(".lc-line-" + ser.id)
            .style("display", hidden ? "none" : null);
    }

    
    function isActionsSuppressed() {
        return eventsDisabled  ||  isTransiting()  ||  loading;
    }

    function resetZoom() {
        if( isActionsSuppressed() ) return;

        let noValChange = valmin===chart.valmin  &&  valmax===chart.valmax;

        valmin = chart.valmin;
        valmax = chart.valmax;

        const mid = dtbeg.getTime() + (dtend.getTime() - dtbeg.getTime()) / 2;
        const half = chart.period * 1000 / 2;

        let dt1 = new Date(dtbeg);
        let dt2 = new Date(dtend);
        dtbeg.setTime(mid - half);
        dtend.setTime(mid + half);

        noDtChange = dtbeg.getTime()===dt1.getTime()  &&  dtend.getTime()===dt2.getTime();

        if( noValChange  &&  noDtChange )
            return;

        updateZoom(noDtChange);
    }


    function setPeriod(period) {
        if( isActionsSuppressed() ) return;

        let half = (dtend.getTime() - dtbeg.getTime()) / 2;
        let mid = dtbeg.getTime() + half;
        half = period * 1000 / 2;
        dtbeg.setTime(mid - half);
        dtend.setTime(mid + half);

        updateZoom();
    }


    function goto(dt) {
        if( isActionsSuppressed() ) return;

        let mid = dtConvT(dt).getTime();
        let half = (dtend.getTime() - dtbeg.getTime()) / 2;
        dtbeg.setTime(mid - half);
        dtend.setTime(mid + half);

        translate();
    }


    function move(kx, ky) {
        if( isActionsSuppressed() ) return;

        let per = (valmax - valmin) * ky;
        valmin += per;
        valmax += per;

        per = (dtend.getTime() - dtbeg.getTime()) * kx;
        dtbeg.setTime(dtbeg.getTime() + per);
        dtend.setTime(dtend.getTime() + per);

        translate();
    }


    function moveToHovers() {
        if( isActionsSuppressed() ) return;

        updateHoverValues();

        let half = (valmax - valmin)/2;
        valmin = hoverVal - half;
        valmax = hoverVal + half;

        half = (dtend.getTime() - dtbeg.getTime())/2;
        dtbeg.setTime(hoverDt.getTime() - half);
        dtend.setTime(hoverDt.getTime() + half);

        translate();
    }

    function isTransiting() {
        return d3.active(xAxisG.node()) !== null;
    }

    function translate() {
        let deltaX = 0 - xScale(dtbeg);
        let deltaY = 0 - yScale(valmax);

        setScales();
        scaleAxises(true);

        svg.selectAll(".lc-line").select("path")
            .transition()
            .duration(transitDuration)
            .on("end", deltaX===0? interruptAndRedrawLines: reloadLines)
            .attr("transform", "translate(" + deltaX + "," + deltaY + ")")

        saveEnvironment();
    }


    function zoom(kx, ky) {
        if( isActionsSuppressed() ) return;

        let half = (valmax - valmin) / 2;
        let mid = valmin + half;
        half = half * ky;
        valmin = mid - half;
        valmax = mid + half;

        half = (dtend.getTime() - dtbeg.getTime()) / 2;
        mid = dtbeg.getTime() + half;
        half = half * kx;
        dtbeg.setTime(mid - half);
        dtend.setTime(mid + half);

        let noReload = kx === 1;
        updateZoom( noReload );
    }

    function zoomIn() {
        zoom(
            zoomDir.includes("x")? 0.5: 1,
            zoomDir.includes("y")? 0.5: 1
        );
    }

    function zoomOut() {
        zoom(
            zoomDir.includes("x")? 2: 1,
            zoomDir.includes("y")? 2: 1
        );
    }





    function setZoomDir(dir) {
        if (eventsDisabled) return;

        zoomDir = dir;
        setBrush();

        saveEnvironment();
    }

    function toggleZoomDir(el) {
        if (eventsDisabled) return;

        if (zoomDir === "x")
            zoomDir = "y";
        else if (zoomDir === "y")
            zoomDir = "xy";
        else if (zoomDir === "xy")
            zoomDir = "x";

        el.innerText = zoomDir;
        setBrush();

        saveEnvironment();
    }

    let brush;

    function setBrush() {
        if( mobile ) return;

        if (zoomDir === "x")
            brush = d3.brushX();
        if (zoomDir === "y")
            brush = d3.brushY();
        if (zoomDir === "xy")
            brush = d3.brush();

        brush.extent([[0, 0], [width, height]])
            .on("start", onBrushStart)
            .on("end", onBrushEnd);

        context.call(brush);

    }


    let brushStartX = 0;
    let brushStartY = 0;

    function onBrushStart() {
        brushStartX = hoverX;
        brushStartY = hoverY;

        setHoverVisible(false);
    }

    function onBrushEnd() {
        const sel = d3.event.selection;
        if (!sel)
            return;

        context.call(brush.move, null);

        updateHovers();

        if (Math.abs(hoverX - brushStartX) < 10  &&  Math.abs(hoverY - brushStartY) < 10)
            return;

        if (hoverX < brushStartX && hoverY < brushStartY) {
            resetZoom();
            return;
        }

        if (zoomDir === "x") {
            dtbeg = xScale.invert(sel[0]);
            dtend = xScale.invert(sel[1]);
        } else if (zoomDir === "y") {
            valmin = yScale.invert(sel[1]);
            valmax = yScale.invert(sel[0]);
        } else if (zoomDir === "xy") {
            dtbeg = xScale.invert(sel[0][0]);
            dtend = xScale.invert(sel[1][0]);
            valmin = yScale.invert(sel[1][1]);
            valmax = yScale.invert(sel[0][1]);
        }

        updateZoom();
    }


    function updateZoom(noReload = false) {
        updateBounds();
        setScales();
        scaleAxises(true);
        svg.selectAll(".lc-line").select("path")
            .transition()
            .duration(transitDuration)
            .on("end", noReload? interruptAndRedrawLines: reloadLines)
            .attr("d", d => line(d.data))

        saveEnvironment();
    }


    function reloadLines() {
        if( loading ) return;
        loadLines();
        updateHoverValues();
    }

    function interruptAndRedrawLines() {
        svg.selectAll(".lc-line path").interrupt();

        svg.selectAll(".lc-line").select("path")
            .attr("transform", "translate(0,0)")
            .attr("d", d => line(d.data))
    }


    let bounds;
    function updateBounds() {
        bounds = {};
        svg.selectAll(".lc-line").select("path").each(d => {
            let lo = binarySearchBound(false, d.data, dtbeg, d => d.dt);
            let hi = binarySearchBound(true, d.data, dtend, d => d.dt);

            let found = {};
            if (lo >= 0 && lo < d.data.length) {
                found['lo'] = d.data[lo];
            }

            if (hi >= 0 && hi < d.data.length) {
                found['hi'] = d.data[hi];
            }

            bounds[d.id] = found;
        })
    }


    function binarySearchBound(upper, arr, item, cast) {
        let lo = -1, hi = arr.length;
        const pred = upper ? (a, b) => a >= b : (a, b) => a > b;
        while (1 + lo < hi) {
            const mi = lo + ((hi - lo) >> 1);
            if (pred(cast(arr[mi]), item)) {
                hi = mi;
            } else {
                lo = mi;
            }
        }
        return upper ? hi : lo;
    }


// ===== hover lines procs =====
    function setXHoverVisible(value) {
        if (xHoverVisible !== value) {
            xHoverVisible = value;
            let v = value ? "visible" : "hidden";
            xHoverLine.style("visibility", v);
            xLabelG.style("visibility", v);
        }
    }

    function setYHoverVisible(value) {
        if (yHoverVisible !== value) {
            yHoverVisible = value;
            let v = value ? "visible" : "hidden";
            yHoverLine.style("visibility", v);
            yLabelG.style("visibility", v);
        }
    }

    function setHoverVisible(value) {
        setXHoverVisible(value);
        setYHoverVisible(value);
    }

    function updateHovers() {
        hoverX = d3.mouse(svg.node())[0];
        hoverY = d3.mouse(svg.node())[1];

        setXHoverVisible(hoverX > 0 && hoverX < width);
        setYHoverVisible(hoverY > 0 && hoverY < height);

        posHovers();
    }

    function posHovers() {
        if (xHoverVisible)
            xHoverLine.attr("transform", "translate(" + hoverX + ",0)");

        if (yHoverVisible)
            yHoverLine.attr("transform", "translate(0, " + hoverY + ")")
    }

    let xLabelRectWidth = 0;
    function updateHoverValues() {
        hoverDt = xScale.invert(hoverX);
        hoverVal = yScale.invert(hoverY);

        // hover label
        if (xHoverVisible) {
            if (!isDragging)
                xLabel.text(formatDt(hoverDt));

            let len = xLabel.node().getComputedTextLength();

            xLabelRectWidth = Math.max( xLabelRectWidth, len+8 );
            let half = xLabelRectWidth / 2;
            xLabelRect
                .attr("width", xLabelRectWidth)
                .attr("x", -half);

            let x = hoverX;
            x = Math.max(half, x);
            x = Math.min(x, width - half);

            xLabelG.attr("transform", "translate(" + x + ",0)")

        }

        if (yHoverVisible) {
            let y = hoverY;
            yLabelG.attr("transform", "translate(0," + y + ")");

            if (!isDragging)
                yLabel.text( formatVal(hoverVal) );
        }
    }


// ===== drag =====
    let dragX, dragY;
    let dragDt, dragVal;
    let isDragging = false;

    function startDrag() {
        if (isDragging  ||  mobile) return;
        isDragging = true;
        dragX = hoverX;
        dragY = hoverY;

        dragDt = hoverDt;
        dragVal = hoverVal;

        context.select(".overlay")
            .attr("cursor", "pointer");
    }

    function finishDrag() {
        if (!isDragging) return;
        isDragging = false;

        context.select(".overlay")
            .attr("cursor", "crosshair");

        svg.selectAll(".lc-line").select("path")
            .attr("transform", "translate(0,0)")
            .attr("d", d => line(d.data));

        updateBounds();
        loadLines();
        saveEnvironment();
    }

    function drag() {
        if (!isDragging) return;

        let deltaDt = hoverDt - dragDt;
        dtbeg.setTime(dtbeg.getTime() - deltaDt);
        dtend.setTime(dtend.getTime() - deltaDt);

        let deltaVal = hoverVal - dragVal;
        valmin = valmin - deltaVal;
        valmax = valmax - deltaVal;

        setScales();
        scaleAxises();

        let deltaX = hoverX - dragX;
        let deltaY = hoverY - dragY;
        svg.selectAll(".lc-line").select("path")
            .attr("transform", "translate(" + deltaX + "," + deltaY + ")")
    }


// ===== load and draw lines =====
    let getUrlDateFull = () => {
        let period = dtend - dtbeg;
        let dt = new Date();

        dt.setTime(dtbeg.getTime() - period);
        let sdtbeg = formatDtT(dt);

        dt.setTime(dtend.getTime() + period);
        let sdtend = formatDtT(dt);

        return `${urlData}?name=${chart.name}&dtbeg=${sdtbeg}&dtend=${sdtend}`;
    };

    let convRowData = (d) => {
        let dt = dtConv(d.dt);
        return {id: +d.id, dt, val: +d.val};
    };

    let loading = false;
    function loadLines() {
        // console.log("start loading");

        loading = true;
        showStatusLoading();


        let url = getUrlDateFull();
        // console.log(url);
        const dataset = d3.csv(url, convRowData);

        dataset.then(function (data) {


            chart.series.forEach(ser => ser.data = []);
            if (chart.series.length > 0) {
                let i = 0;
                let id = chart.series[i].id;
                data.forEach(d => {
                    if (d.id !== id) {
                        id = d.id;
                        i = chart.series.findIndex(el => el.id === id);
                    }
                    if (i >= 0)
                        chart.series[i].data.push({dt: d.dt, val: d.val});
                });
            }


            if (bounds) {
                chart.series.forEach(ser => {
                    let bnd = bounds[ser.id];
                    if (bnd) {
                        if (bnd.lo && (ser.data.length === 0 || bnd.lo.dt < ser.data[0].dt))
                            ser.data.unshift(bnd.lo);
                        if (bnd.hi && (ser.data.length === 0 || bnd.hi.dt > ser.data[ser.data.length - 1].dt))
                            ser.data.push(bnd.hi);
                    }
                });
                bounds = null;
            }


            svg.selectAll(".lc-line")
                .data(chart.series)
                .enter()
                .append('g')
                .attr("clip-path", "url(#linechart-clip)")
                .attr("class", ser => "lc-line lc-line-" + ser.id)
                .append("path");

            svg.selectAll(".lc-line")
                .data(chart.series)
                .exit().remove();


            let glines = svg.selectAll(".lc-line")
                .data(chart.series)
                .style("display", ser => ser.hidden ? "none" : null)
                .select("path");


            glines
                .attr("fill", "none")
                .attr("stroke", ser => ser.color)
                .attr("stroke-width", ser => ser.width);

            glines
                .attr("transform", "translate(0,0)")
                .attr("d", ser => line(ser.data));

            showStatus();

            loading = false;
        })

        .catch(err => {
            showStatusError(err);
            loading = false;
        });

    }


    function showStatus(msg, error) {
        statusSel
            .style("visibility", msg===undefined? "hidden": "visible")
            // .classed("lc-status-ok", !error)
            .classed("lc-status-error", error)
            .text(msg);
    }

    function showStatusLoading() {
        showStatus("Загрузка данных");
    }

    function showStatusError(err) {
        showStatus("Ошибка загрузки данных! " + err, true);
    }



// Must be called after chart config loaded
    function initChart() {
        chart.series.forEach((ser, i) => {
            ser.data = [];
            ser.hidden = false;

            if (ser.width === undefined || ser.width < 0.5)
                ser.width = 1.5;

            if (ser.color === undefined || ser.color === "")
                ser.color = i < colorList.length ? colorList[i] : "black";

            if (ser.id === 0)
                ser.id = -1 * i;
        });

        yAxisG.select(".lc-y-axis-units").text(chart.units);

        titleSel
            .text(chart.title)
            .style("visibility", "visible");
            // .style("opacity", "100");

        if (chart.precision === undefined)
            chart.precision = 1;

    }


// ===== save and load =====
    function saveEnvironment() {
        let group = "lc." + chart.name + ".";
        localStorage.setItem(group + "dtbeg", formatDtT(dtbeg));
        localStorage.setItem(group + "dtend", formatDtT(dtend));
        localStorage.setItem(group + "valmin", valmin);
        localStorage.setItem(group + "valmax", valmax);
    }


    function loadEnvironment() {
        let group = "lc." + chart.name + ".";
        let v;

        v = localStorage.getItem(group + "dtend");
        dtend = v !== null ? dtConvT(v) : new Date();

        v = localStorage.getItem(group + "dtbeg");
        if (v !== null)
            dtbeg = dtConvT(v);
        else {
            dtbeg = new Date();
            dtbeg.setTime(dtend.getTime() - chart.period * 1000);
        }

        v = localStorage.getItem(group + "valmin");
        valmin = v !== null ? +v : chart.valmin;

        v = localStorage.getItem(group + "valmax");
        valmax = v !== null ? +v : chart.valmax;
    }


    function loadChart(name) {
        removeEvents();
        setHoverVisible(false);

        // clear view
        yAxisG.select(".lc-y-axis-units").text("");

        // svg.select(".lc-title")
        titleSel
            .style("visibility", "hidden");
            // .style("opacity", "0");

        svg.selectAll(".lc-line")
            .remove();

        legendTable.selectAll("tr")
            .remove();

        showStatusLoading();


        let req = d3.json(urlConfig + "?name=" + name);
        req.then(d => {
            chart = d;

            initChart();
            loadEnvironment();
            setBrush();
            setScales();
            scaleAxises();
            drawLegend();
            adjustSize();
            loadLines();
            installEvents();
        })
            .catch(err => {
                showStatusError(err);
            });
    }


/////////// test //////////////
    function testLoadEnvironment() {
        dtbeg = dtConv('200128000000');
        dtend = dtConv('200202000000');
        valmin = 0;
        valmax = 16000;
    }


    function testLoadChart(name) {
        console.log("TEST loadLineChart config from server: " + name);

        removeEvents();

        // ---- test data ----
        chart.valmin = 0;
        chart.valmax = 16000;
        chart.period = 24 * 60 * 60;

        chart.name = name;
        chart.title = "Производительность размольного отделения";
        chart.units = "Производительность, кг/ч";

        chart.series = [
            {
                id: 1,
                descr: "Мука В/с",
                width: 2
            },
            {
                id: 2,
                descr: "Мука 1/с",
                color: "green",
                width: 1
            },
            {
                id: 3,
                descr: "Мука 2/с",
                color: "blue"
            },
            {
                id: 4,
                descr: "Мука 3/с"
            },
            {
                id: 5,
                descr: "Мука 4/с"
            }
        ];

        getUrlDateFull = () => "sample.csv";

        convRowData = (d) => {
            let dt = dtConv(d.dt);

            if (dt < dtbeg || dt > dtend)
                return null;
            return {id: +d.id, dt, val: (+d.val) / 1000};
        }


        initChart();
        testLoadEnvironment();
        setBrush();
        setScales();
        scaleAxises();
        drawLegend();
        adjustSize();
        loadLines();
        installEvents();
    }

    return {
        resize,
        adjustSize,
        goto,
        setPeriod,
        move,
        zoom,
        zoomIn,
        zoomOut,
        resetZoom,
        toggleZoomDir,
        loadChart,
        testLoadChart

    }

}