const maxReportPageSize = 2097152;

let reportUrl = "";
let reportName = "";
let reportPage;
let reportParamBar;
let reportContent;

function onReportClick(sender) {
    activatePage("report-page");
    applyReportZoom();

    reportName = sender.getAttribute("data-report-name");

    const params = sender.querySelector("div");
    reportParamBar.innerHTML = params.innerHTML;

    restoreReportValues();

    reportContent.innerText = "";
    window.scrollTo(0, 0);

    showReport();
}

function showReport() {
    saveReportValues();

    const params = reportParamBar.querySelectorAll("input,select");
    reportUrl = "report/show?report=" + reportName;
    for(param of params)
        if( param.value )
            reportUrl += "&" + param.id + "=" + param.value;

    loadReport();
}

async function loadReport() {
    const btUpd = document.querySelector("#report-bt-update");
    try {
        btUpd.disabled = true;
        const res = await axios.get(reportUrl);

        if( res.data.length > maxReportPageSize) {
            reportContent.innerHTML = reportPage.querySelector(".report-toobig").outerHTML
                + res.data.substring(0, maxReportPageSize);

            reportContent.querySelectorAll("table tbody tr td[width='50%']")
                .forEach(el => el.remove());

        } else {
            reportContent.innerHTML = res.data;
        }

    } catch (error) {
        reportContent.innerHTML = reportPage.querySelector(".report-error").outerHTML;
        reportContent.querySelector(".exception").innerText = error;
    }

    btUpd.disabled = false;
}


function exportReport(exportFormat) {
    window.open(reportUrl + "&export=" + exportFormat,'_blank');
}

function exportPdf() {
    exportReport("pdf");
}


function getReportSaveId() {
    return "report." + reportName + ".";
}

function saveReportValues() {
    let group = getReportSaveId();
    for(prm of reportParamBar.querySelectorAll(".saveable")) {
        localStorage.setItem(group + prm.id, prm.value);
    }
}

function restoreReportValues() {
    let group = getReportSaveId();
    for(prm of reportParamBar.querySelectorAll(".saveable")) {
        let value = localStorage.getItem(group + prm.id);
        if( value != undefined )
            prm.value = value;
    }
}


function addDays(prmName, days) {
    const prm = reportParamBar.querySelector("#"+prmName);
    if(prm !== null ) {
        const date = new Date(prm.value);
        date.setDate(date.getDate() + days);
        prm.value = date.toISOString().slice(0,10);
    }
}

let reportZoom = 1;
function saveReportZoom() {
    localStorage.setItem("reportZoom", reportZoom);
}

function restoreReportZoom() {
    reportZoom = +localStorage.getItem("reportZoom");
    if( !reportZoom  )
        reportZoom = 1;
    applyReportZoom();
}

function reportZoomIn() {
    reportZoom += 0.1;
    applyReportZoom();
    saveReportZoom();
}

function reportZoomOut() {
    reportZoom -= 0.1;
    if( reportZoom < 1 )
        reportZoom = 1;
    applyReportZoom();
    saveReportZoom();
}

function applyReportZoom() {
    if( reportZoom < 0.1 ) return;

    let cont = document.querySelector(".report-content");

    cont.style.transform = "scale(" + reportZoom + ")";
    let adjWidth = cont.parentElement.clientWidth / reportZoom;
    cont.style.width = adjWidth + "px";
    // console.log('aplly report zoom:',  adjWidth, reportZoom);
}


// workaround for chrome repaint bug
// let timer;
// function reportResize() {
//     if(timer)
//         clearTimeout(timer);
//     timer = setTimeout(() => {
//         console.log("fixing bug");
//         timer = null;
//
//         let navbar = document.querySelector("#report-page .navbar");
//         console.log(navbar.style.display);
//         navbar.style.display='none';
//         navbar.offsetHeight;
//         navbar.style.display='block';
//
//     }, 1000);
// }

document.addEventListener("DOMContentLoaded", ()=>{
    reportPage = document.querySelector(".report-page");
    reportParamBar = reportPage.querySelector("#report-parambar");
    reportContent = reportPage.querySelector(".report-content");

    d3.selectAll(".report-link").on("click", function () {
        onReportClick(this);
    });

    document.querySelector("#report-bt-pdf").onclick = exportPdf;
    document.querySelector("#report-bt-update").onclick = showReport;

    document.querySelector("#report-zoom-in").onclick = reportZoomIn;
    document.querySelector("#report-zoom-out").onclick = reportZoomOut;
    restoreReportZoom();

    addMainResizeListener(() => {
        if(isPageActive("report-page"))
            applyReportZoom();
    });



});



