let lc;

document.addEventListener("DOMContentLoaded", ()=> {
    lc = createLineChart(".chart-page", "chart/config", "chart/data");

    addMainResizeListener(() => {
        if(isPageActive("chart-page"))
            lc.adjustSize();
    });

    d3.selectAll(".chart-link").on("click", function () {
        activatePage("chart-page");
        lc.loadChart(this.getAttribute("data-chart-name"));
    });
});

