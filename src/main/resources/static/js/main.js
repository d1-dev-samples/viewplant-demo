function openFullscreen() {
    const elem = document.documentElement;
    if (elem.requestFullscreen) {
        elem.requestFullscreen();
    } else if (elem.mozRequestFullScreen) { /* Firefox */
        elem.mozRequestFullScreen();
    } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
        elem.webkitRequestFullscreen();
    } else if (elem.msRequestFullscreen) { /* IE/Edge */
        elem.msRequestFullscreen();
    }
}

function closeFullscreen() {
    const elem = document.documentElement;
    if (document.exitFullscreen) {
        document.exitFullscreen();
    } else if (document.mozCancelFullScreen) { /* Firefox */
        document.mozCancelFullScreen();
    } else if (document.webkitExitFullscreen) { /* Chrome, Safari and Opera */
        document.webkitExitFullscreen();
    } else if (document.msExitFullscreen) { /* IE/Edge */
        document.msExitFullscreen();
    }
}

let fullScreen = false;
function toggleFullScreen() {
    if(fullScreen = !fullScreen)
        openFullscreen();
    else
        closeFullscreen();
}


function initTreeViewAll() {
    document.querySelectorAll(".treeview").forEach( root => {

        const map = {};
        Object.values(root.children)
            .map(it => {return {"attr": it.attributes["data-tv-id"], "element": it}})
            .filter(it => it.attr !== undefined)
            .filter(it => +(it.attr.value) > 0)
            .forEach(it => map[it.attr.value] = it.element);


        Object.values(map)
            .map(it => {return {"attr": it.attributes["data-tv-parent"], "element": it}})
            .filter(it => it.attr !== undefined)
            .filter(it => map[it.attr.value] !== undefined)
            .map(it => {return {"parent": map[it.attr.value].querySelector("ul"), "element": it.element}})
            .filter(it => it.parent != null )
            .forEach(it => it.parent.appendChild(it.element));


        root.querySelectorAll(".caret").forEach(it => it.addEventListener("click", function () {
            this.parentElement.querySelector(".nested").classList.toggle("nested-opened");
            this.classList.toggle("caret-down");
        }));


        root.hidden = false;
    });
}


let activePage;
function activatePage(page) {
    console.log("activate page: " + page);
    document.querySelectorAll(".page").forEach(el => {
        let visible = el.classList.contains(page);
        el.style.visibility = visible? "visible": "hidden";
        el.hidden = !visible;

        if( visible )
            adjustPage(el);
    });
    activePage = page;
}

function adjustPage(pageEl) {
    // set padding-right for navbarfx-left
    pageEl.querySelectorAll(".navbarfx-wrap").forEach( el => {
        const left = el.querySelector(".navbarfx-left");
        const right = el.querySelector(".navbarfx-right > div > div"); //todo: needs get refactored!
        left.style.paddingRight = (right.clientWidth + 4) + "px";
    });

    // set marging for navbarfx-left
    pageEl.querySelectorAll(".navbarfx").forEach( el => {
        el.nextElementSibling.style.marginTop = el.clientHeight + "px";
    });

}

function isPageActive(page) {
    return activePage === page;
}

function openSidenav() {
    const sidenav = document.querySelector(".sidenav");
    const main = document.querySelector(".main");
    const hidetab = document.querySelector(".hidetab");
    const sidenavWidth = sidenav.clientWidth;

    hidetab.classList.remove("hidetab-closed");
    sidenav.style.transform = `translateX(0px)`;
    main.style.left = sidenavWidth + "px";

    executeMainResizeListeners();
}

function toggleSidenav(noTransition) {
    const sidenav = document.querySelector(".sidenav");
    const main = document.querySelector(".main");
    const hidetab = document.querySelector(".hidetab");
    const sidenavWidth = sidenav.clientWidth;

    hidetab.classList.toggle("hidetab-closed");
    const closed = hidetab.classList.contains("hidetab-closed");

    sidenav.style.transition = noTransition===true? "0s": "0.4s";
    sidenav.style.transform = `translateX(-${closed? sidenavWidth: 0}px)`;
    main.style.left = (closed? 0: sidenavWidth) + "px";

    executeMainResizeListeners();
}


function logoutUser() {
    document.logoutForm.submit();
}


const mainResizeListeners = [];
function addMainResizeListener(callback) {
    mainResizeListeners.push(callback);
}

function executeMainResizeListeners() {
    for(callback of mainResizeListeners)
        callback();
}


// async
function loadWelcomePage() {
    // const welcomeUrl = contextRoot + "welcome";
    const welcomeUrl = "welcome";
    const container = document.querySelector(".welcome-page");

    // try {
    //     const res = await axios.get(welcomeUrl);
    //     container.innerHTML = res.data;
    // } catch (error) {
    //     container.innerText = error;
    // }

    $('.welcome-page').load(welcomeUrl);

    activatePage("welcome-page");
}


document.addEventListener("DOMContentLoaded", ()=>{
    initTreeViewAll();

    const sidenav = document.querySelector(".sidenav");

    document.querySelector(".sidenav-logo").onclick = () => loadWelcomePage();
    document.querySelector(".hidetab").onclick = toggleSidenav;

    const btFullscreen = document.querySelector(".sidenav-fullscreen");
    if( isMobile.any )
        btFullscreen.onclick = toggleFullScreen;
    else
        btFullscreen.hidden = true;

    document.querySelector(".treeview-wrap").style.top =
    document.querySelector(".sidenav-header").clientHeight + "px";

    window.addEventListener('resize', executeMainResizeListeners);

    toggleSidenav(true);

    loadWelcomePage();
});




