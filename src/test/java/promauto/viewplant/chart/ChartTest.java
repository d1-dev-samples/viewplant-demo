package promauto.viewplant.chart;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ChartTest {

    Chart chart;
    ChartSerie ser1;
    ChartSerie ser2;

    @BeforeEach
    void setUp() {
        chart = new Chart("");
        ser1 = new ChartSerie("ser1", 0, "", "col1", 1.0);
        ser2 = new ChartSerie("ser2", 0, "", "col2", 2.0);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void prepareSqlInit_1() {
        chart.setSqlInit("sqlinit");

        chart.prepareSqlInit();
        assertEquals("sqlinit", chart.getSqlInit());
    }

    @Test
    void prepareSqlInit_2() {
        chart.setSqlInit("sqlinit");
        chart.addSerie(ser1);
        chart.addSerie(ser2);

        chart.prepareSqlInit();
        assertEquals("sqlinit", chart.getSqlInit());
    }

    @Test
    void prepareSqlInit_3() {
        chart.setSqlInit("sqlinit");
        ser1.setSqlInit("sqlinit1");
        chart.addSerie(ser1);
        chart.addSerie(ser2);

        chart.prepareSqlInit();
        assertEquals("sqlinit union sqlinit1", chart.getSqlInit());
    }

    @Test
    void prepareSqlInit_4() {
        chart.setSqlInit("sqlinit");
        ser1.setSqlInit("sqlinit1");
        ser2.setSqlInit("sqlinit2");
        chart.addSerie(ser1);
        chart.addSerie(ser2);

        chart.prepareSqlInit();
        assertEquals("sqlinit union sqlinit1 union sqlinit2", chart.getSqlInit());
    }

    @Test
    void prepareSqlInit_5() {
        ser1.setSqlInit("sqlinit1");
        ser2.setSqlInit("sqlinit2");
        chart.addSerie(ser1);
        chart.addSerie(ser2);

        chart.prepareSqlInit();
        assertEquals("sqlinit1 union sqlinit2", chart.getSqlInit());
    }



}